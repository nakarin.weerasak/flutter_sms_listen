import 'package:flutter/material.dart';
import 'package:sms/sms.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Welcome to Flutter'),
        ),
        body: AppStateFul(),
      ),
    );
  }
}

class AppStateFul extends StatefulWidget {
  AppStateFul({Key key}) : super(key: key);

  @override
  _AppStateFulState createState() => _AppStateFulState();
}

class _AppStateFulState extends State<AppStateFul> {
  SmsReceiver receiver = new SmsReceiver();
  String smsText = "Pending..";

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          smsText,
          style: TextStyle(color: Colors.blueAccent),
        ),
        SizedBox(
          height: 20,
        ),
        Center(
          child: ElevatedButton(
            child: Text('Start Listen SMS'),
            onPressed: () {
              print('get****');
              listenSMS();
            },
          ),
        ),
      ],
    ));
  }

  Future<void> listenSMS() async {
    receiver.onSmsReceived.listen((SmsMessage msg) => {
          setState(() {
            smsText = msg.body;
          })
        });
  }
}
